import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Main from '../screens/Main';
import Details from '../screens/Details';

const stack = createStackNavigator({
    Main: {
        screen: Main,
    },
    Details: {
        screen: Details,
    },
});

export default createAppContainer(stack);
