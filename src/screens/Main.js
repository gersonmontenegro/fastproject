import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const Main = (props) => {
    const go2Details = () => {
        const { navigation } = props;
        navigation.navigate('Details');
    }

    return (
        <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to Fastlane!</Text>
        <TouchableOpacity onPress={go2Details}>
          <Text style={styles.instructions}>Go to next screen</Text>
          <Text>¿1s J3nkins w0rking?</Text>
          <Text>187</Text>
        </TouchableOpacity>
      </View>
    );
}

const styles = {
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
};
  
export default Main;
