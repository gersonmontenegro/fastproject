import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const Details = () => {
    return (
        <View style={styles.container}>
        <Text style={styles.welcome}>Detail page!</Text>
      </View>
    );
}

const styles = {
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
};
  
export default Details;
